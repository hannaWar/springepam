package com.war.epam_spring;

import java.util.List;

public class Statistic<T> {
    private int inputNum;
    private int errorNum;
    private T min, max;
    private T popularResult;
    private List<T> response;

    public List<T> getResponse() {
        return response;
    }

    public void setResponse(List<T> response) {
        this.response = response;
    }

    public int getInputNum() {
        return inputNum;
    }

    public void setInputNum(int inputNum) {
        this.inputNum = inputNum;
    }

    public int getErrorNum() {
        return errorNum;
    }

    public void setErrorNum(int errorNum) {
        this.errorNum = errorNum;
    }

    public T getMin() {
        return min;
    }

    public void setMin(T min) {
        this.min = min;
    }

    public T getMax() {
        return max;
    }

    public void setMax(T max) {
        this.max = max;
    }

    public T getPopularResult() {
        return popularResult;
    }

    public void setPopularResult(T popularResult) {
        this.popularResult = popularResult;
    }
}
