package com.war.epam_spring.counter;

import org.springframework.stereotype.Component;

@Component
public class Counter {

    private int count;
//TODO
    public synchronized int getCount() {
        return count;
    }

    public synchronized void increment(){
        count++;
    }
}
