package com.war.epam_spring;

import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.lang.reflect.Method;

@SpringBootApplication
@EnableAsync//включает асинхронную работу
public class EpamSpringApplication implements WebMvcConfigurer {

    public static void main(String[] args) {
        SpringApplication.run(EpamSpringApplication.class, args);
    }




}
