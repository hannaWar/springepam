package com.war.epam_spring.shift;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.stream.Stream;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class CShifter implements IShifter {
//TODO:
    public static final int SIZE = 26;//////////
    char[] lowerCase = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
            'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u',
            'v', 'w', 'x', 'y', 'z'};
    char[] upperCase = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
            'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
            'X', 'Y', 'Z'};
    private int step = 10;

    @Override
    public String encrypt(String string) {
        char[] textChar = string.toCharArray();
        for (int i = 0; i < textChar.length; i++) {
            int index;
            if (Arrays.binarySearch(lowerCase, textChar[i]) != -1 || Arrays.binarySearch(upperCase, textChar[i]) != -1) {
                if (Character.isLowerCase(textChar[i])) {
                    index = Arrays.binarySearch(lowerCase, textChar[i]);
                    index += step;
                    if (index > SIZE) {
                        index -= SIZE;//цикличность
                    }
                    textChar[i] = lowerCase[index];
                } else {
                    index = Arrays.binarySearch(upperCase, textChar[i]);
                    index += step;
                    if (index > SIZE) {
                        index -= SIZE;
                    }
                    textChar[i] = upperCase[index];
                }
            }
        }
        return new String(textChar);

    }

    @Override
    public Stream<String> encrypt(Stream<String> data) {
        return data.parallel().map(this::encrypt);
    }

    @Override
    public Stream<String> decrypt(Stream<String> data) {
        return data.parallel().map(this::decrypt);
    }

    @Override
    public String decrypt(String string) {
        char[] textChar = string.toCharArray();
        for (int i = 0; i < textChar.length; i++) {
            int index;
            if (Arrays.binarySearch(lowerCase, textChar[i]) != -1 || Arrays.binarySearch(upperCase, textChar[i]) != -1) {
                if (Character.isLowerCase(textChar[i])) {
                    index = Arrays.binarySearch(lowerCase, textChar[i]);
                    index -= step;
                    if (index < 0) {
                        index += SIZE;
                    }
                    textChar[i] = lowerCase[index];
                } else {
                    index = Arrays.binarySearch(upperCase, textChar[i]);
                    index -= step;
                    if (index < 0) {
                        index += SIZE;
                    }
                    textChar[i] = upperCase[index];
                }
            }
        }
        return new String(textChar);
    }
}
