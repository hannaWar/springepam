package com.war.epam_spring.shift;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.war.epam_spring.BD.Request;
import com.war.epam_spring.BadStringException;
import com.war.epam_spring.Cache;
import com.war.epam_spring.Statistic;
import com.war.epam_spring.StringModel;
import com.war.epam_spring.counter.Counter;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;


@RestController                                 //@ Controller и @ ResponseBody cервер трогаем за причинные места
public class ShiftController {
    private static long id = System.currentTimeMillis();
    final Cache cache;

    final Counter counter;

    final IShifter shifter;

    Logger logger = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);

    public ShiftController(IShifter shifter, Cache cache, Counter counter) {
        this.shifter = shifter;
        this.counter = counter;
        logger.setLevel(Level.DEBUG);
        logger.debug("Shift Controller created");
        this.cache = cache;
    }

    @Async
    @GetMapping("/count")
    CompletableFuture<Integer> getCount() {
        return CompletableFuture.completedFuture(counter.getCount());
    }

    @Async
    @GetMapping("/encrypt")
        //для обработки get запроса
    CompletableFuture<StringModel> getShiftedString(@RequestParam("value") String value, @RequestParam(name = "pid", required = false) Long pid) { //для извлечения параметров запроса
        logger.info(String.format("ENCRYPTING: \"%s\" ", value));
        counter.increment();
        if (value.isEmpty()) {
            logger.error("Empty String Error");
            throw new BadStringException();
        }
        Request res;
        var model = new StringModel();
        if (pid == null) {
            res = new Request();
            model.setPid(generatorId());
            res.setEncrypted(shifter.encrypt(value));

        } else {
            res = cache.getEncrypted(value, pid);
            model.setPid(pid);
            if (res == null) {
                logger.info(String.format("Not in cache: \"%s\" ", value));
                res = new Request();
                res.setEncrypted(shifter.encrypt(value));
                cache.putEncrypted(value, res.getEncrypted(), pid);
            }
        }

        model.setValue(res.getEncrypted());
        return CompletableFuture.completedFuture(model);
    }

    @Async
    @GetMapping(value = "/decrypt")
    CompletableFuture<StringModel> getDeShiftedString(@RequestParam("value") String value, @RequestParam(name = "pid", required = false) Long pid) {
        logger.info(String.format("DECRYPTING: \"%s\" ", value));
        counter.increment();
        if (value.isEmpty()) {
            logger.error("Empty String Error");
            throw new BadStringException();
        }
        Request res;
        var model = new StringModel();
        if (pid == null) {
            model.setPid(generatorId());
            res = new Request();
            res.setSrc(shifter.decrypt(value));

        } else {
            model.setPid(pid);
            res = cache.getDecrypted(value, pid);
            if (res == null) {
                logger.info(String.format("Not in cache: \"%s\" ", value));
                res = new Request();
                res.setSrc(shifter.decrypt(value));
                cache.putDecrypted(value, res.getSrc(), pid);
            }
        }
        model.setValue(res.getSrc());
        return CompletableFuture.completedFuture(model);
    }

    @Async
    @GetMapping(value = "/pid")
    CompletableFuture<List<Request>> getDeShiftedString(@RequestParam("pid") Long pid) {
        logger.info(String.format("PID: \"%d\" ", pid));
        counter.increment();
        return CompletableFuture.completedFuture(cache.getByPid(pid));
    }

    private synchronized long generatorId() {

        return id++;
    }


    private Statistic<String> getStatistics(List<String> list, List<String> result) {
        String resultString = "";
        int count = 0;
        Statistic<String> statistic = new Statistic<>();
        statistic.setInputNum(list.size());
        statistic.setErrorNum((int) list.stream().filter((s -> s.isBlank())).count());//превращаем список в поток, оставляем только пустые строки и считаем их
        statistic.setMax(result.stream().max(String::compareTo).get());
        statistic.setMin(result.stream().min(String::compareTo).get());
        HashMap<String, Integer> counts = result.stream().collect(
                HashMap::new,
                (map, str) -> {
                    if (map.containsKey(str))
                        map.put(str, map.get(str) + 1);//увеличиваем значение по ключу на 1
                    else
                        map.put(str, 1);
                }, HashMap::putAll);
        for (String key : counts.keySet()) {
            if (counts.get(key) > count) {
                count = counts.get(key);
                resultString = key;
            }
        }
        statistic.setPopularResult(resultString);
        statistic.setResponse(result);
        return statistic;
    }

    @PostMapping("/encrypt")
    Statistic<String> encryptList(@RequestBody List<String> list) {
        counter.increment();
        return getStatistics(list, shifter.encrypt(list.stream()).collect(Collectors.toList()));
    }

    @PostMapping("/decrypt")
    Statistic<String> decryptList(@RequestBody List<String> list) {
        counter.increment();
        return getStatistics(list, shifter.decrypt(list.stream()).collect(Collectors.toList()));
    }

}
