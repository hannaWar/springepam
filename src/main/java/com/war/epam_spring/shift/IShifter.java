package com.war.epam_spring.shift;

import java.util.stream.Stream;

public interface IShifter {

    String encrypt(String string);

    Stream<String> encrypt(Stream<String> data);

    Stream<String> decrypt(Stream<String> data);

    String decrypt(String string);
}
