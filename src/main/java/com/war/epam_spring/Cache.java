package com.war.epam_spring;

import com.war.epam_spring.BD.CacheRepository;
import com.war.epam_spring.BD.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Cache {
    @Autowired
    CacheRepository repository;

    //public Cache(CacheRepository repository) {
    //  this.repository = repository;
    //}


    public Request getEncrypted(String value, Long pid) {
        return repository.findBySrcAndPid(value, pid);
    }

    public Request getDecrypted(String value, Long pid) {
        return repository.findByEncryptedAndPid(value, pid);
    }

    public List<Request> getByPid(Long pid) {
        return repository.findByPid(pid);
    }

    public void putEncrypted(String key, String value, Long pid) {
        Request request = new Request();
        request.setSrc(key);
        request.setEncrypted(value);
        request.setPid(pid);
        repository.save(request);
    }

    public void putDecrypted(String key, String value, Long pid) {
        Request request = new Request();
        request.setSrc(value);
        request.setEncrypted(key);
        request.setPid(pid);
        repository.save(request);
    }
}
