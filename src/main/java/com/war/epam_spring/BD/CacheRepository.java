package com.war.epam_spring.BD;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CacheRepository extends CrudRepository<Request, Long> {
    Request findBySrcAndPid(String src, Long pid);

    List<Request> findByPid(Long pid);

    Request findByEncryptedAndPid(String encrypted, Long pid);
}
