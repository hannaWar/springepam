function encrypt() {
    $.ajax({
        url: `http://localhost:8080/encrypt?value=${$("#to_encrypt").val()}`
    }).done(function (data) {
        $("#to_decrypt").val(data.value)
    }).fail(function (jqXHR, textStatus) {
        alert('Something went wrong: string is empty');
    });
}

function decrypt() {
    $.ajax({
        url: `http://localhost:8080/decrypt?value=${$("#to_decrypt").val()}`
    }).done(function (data) {
        $("#to_encrypt").val(data.value)
    }).fail(function (jqXHR, textStatus) {
        alert('Something went wrong: string is empty');
    });
}