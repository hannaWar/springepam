package com.war.epam_spring;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class EpamSpringApplicationTests {

    static Logger logger = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);

    String BASE_URL = "http://localhost:8080/";

    @Autowired
    MockMvc mockController; //обеспечивает выполнение rest запросов без запуска сервера

    @BeforeAll
    static void init() {
        logger.setLevel(Level.ALL);
    }

    @Test
    void contextLoads() {
    }

    @Test
    void restTest() throws Exception {
        mockController.perform(get(BASE_URL))   //выполнить запрос по адресу   ожидать,  что ошибки нет
                .andExpect(status().is2xxSuccessful())
                .andReturn();
    }

    @Test
    void encryptionTest() throws Exception {
        var result = mockController.perform(
                get(BASE_URL + "encrypt?value=")
        ).andExpect(request().asyncStarted()).andReturn();

        mockController.perform(
                asyncDispatch(result))
                .andExpect(status().is4xxClientError())
                .andReturn();
    }

    @Test
    void encryptionSuccessTest() throws Exception {
        ObjectMapper mapper = new ObjectMapper();//класс для сооотношения типов данных, преобразует в нашем случае соотносит с json
        var model = new StringModel();
        model.setValue("klm");

        var result = mockController.perform(
                get(BASE_URL + "encrypt")
                        .param("value", "abc"))
                .andExpect(request().asyncStarted()).andReturn();

        mockController.perform(
                asyncDispatch(result))//жду результата запроса
                .andExpect(content().string(mapper.writeValueAsString(model)))
                .andReturn();

    }

    @Test
    void decryptionTest() throws Exception {

        var response = mockController.perform(
                get(BASE_URL + "decrypt?value=")
        ).andExpect(request().asyncStarted())//ожидать
                .andReturn();


        mockController.perform(
                asyncDispatch(response))
                .andExpect(status().is4xxClientError())
                .andReturn();

    }

    @Test
    void decryptionSuccessTest() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        var model = new StringModel();
        model.setValue("abc");

        var result = mockController.perform(
                get(BASE_URL + "decrypt")
                        .param("value", "klm"))
                .andExpect(request().asyncStarted()).andReturn();


        mockController.perform(asyncDispatch(result))
                .andExpect(content().string(mapper.writeValueAsString(model)))
                .andReturn();

    }


}
